/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';

import Main from './components/MainComponent';

export default class App extends React.Component {
  render() {
    StatusBar.setBarStyle('light-content', true);
    return (
        <Main/>
      );
    }
}
