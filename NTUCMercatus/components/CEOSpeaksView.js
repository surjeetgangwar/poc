import React, { Component } from 'react';
import { View, FlatList, StyleSheet, Text, Image, Alert, SafeAreaView } from 'react-native';
import { ListItem, Card, Icon } from 'react-native-elements';

import { POSTS } from '../shared/Posts';

class CEOSpeaksView extends Component {

  constructor(props) {
    super(props)
    this.state = {posts: POSTS}
  };

  static navigationOptions = {
    title: 'CEO Speaks'
  };

  _onLikeClick() {
    Alert.alert('Liked this post');
  }

  _onCommentClick() {
    Alert.alert('Comment on this post');
  }

  render() {
    const {navigate} = this.props.navigation

    const renderMenuItem = ({item, index}) => {
        return (
          <View style = {styles.ListItemContainer}>
            <View style = {{flex: 1, flexDirection: 'row', height: 60, alignItems: 'center'}}>
                <Image source={require('./images/transparent-logo.png')} style={styles.TopIcon}/>
                <Text style = {styles.TitleLabel}> {item.name}</Text>
                <Icon name="clock-o" size={24}
                      color= 'gray'
                      type= 'font-awesome'
                      style={{padding: 2}}
                      onPress={ this._onLikeClick } />
                <Text style = {styles.FooterText}> Just now</Text>
            </View>
            <View style={{ borderBottomColor: 'gray', borderBottomWidth: 1}}/>
            <Image source={require('./images/img1-1.jpg')} style={styles.ImageView}/>
            <Text style = {styles.DescriptionText}>{item.description}</Text>
            <View style = {styles.ListItemFooter}>
                  <View style = {{flex: 0.95}}></View>
                  <Icon name="heart" size={24}
                        color= 'gray'
                        type= 'font-awesome'
                        style={{paddingHorizontal: 10}}
                        onPress={ this._onLikeClick } />
                  <Text style = {styles.FooterText}> {item.likes}</Text>
                  <Icon name="comment" size={24}
                        color= 'gray'
                        style={{paddingHorizontal: 10}}
                        onPress={ this._onCommentClick } />
                  <Text style = {styles.FooterText}> {item.commnets}</Text>
            </View>
          </View>
        );
      };

      return (
          <SafeAreaView>
          <FlatList
              data={this.state.posts}
              renderItem={renderMenuItem}
              keyExtractor={item => item.id.toString()}
          />
          </SafeAreaView>
      );
    }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    justifyContent: 'center',
	},
  ListItemContainer: {
    flex: 1,
    backgroundColor: '#fff',
    margin: 15,
    borderRadius: 5,
    width: '92%',
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 2,
    shadowOpacity: 0.2,
  },
  TopIcon: {
    marginTop: -35,
    width: 60,
    height: 60,
  },
  TitleLabel: {
    fontSize: 23,
    margin: 10,
    flex: 0.9,
  },
  ImageView: {
    flex: 1,
    margin: 20,
    width: 'auto',
    height: 190,
    resizeMode: 'cover',
  },
  DescriptionText: {
    fontSize: 18,
    paddingHorizontal: 20,
    paddingBottom: 20,
    textAlign: 'left',
  },
  ListItemFooter: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#D4D4D4',
    height: 35,
    alignItems: 'center',
  },
  FooterText: {
    fontSize: 15,
    textAlign: 'auto',
  },
});

export default CEOSpeaksView;
