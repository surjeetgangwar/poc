import React, {Component} from 'react';
import { View,
          Platform,
          Text,
          TextInput,
          Image,
          StyleSheet,
          Button,
          Alert,
          Animated,
          TouchableHighlight
      } from 'react-native';

import AnimateLoadingButton from 'react-native-animate-loading-button';

// var isHidden = true;
export default class Login extends Component {

  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      bounceValue: new Animated.Value(200),  //This is the initial position of the subview
    }
  };

  componentDidMount() {
    this._toggleSubview()
  }

  _toggleSubview() {
    var toValue = 0;
    Animated.spring(
      this.state.bounceValue,
      {
        toValue: toValue,
        velocity: 2,
        tension: 2,
        friction: 8,
      }
    ).start();
  }

// Login button action
  _onLoginClick() {
    this.loadingButton.showLoading(true);

    setTimeout(() => {
      this.loadingButton.showLoading(false);
      this.props.navigation.navigate('Home');
    }, 2000);
  }

  render() {
    return (
      <View style={styles.container}>
        <Image source={require('./images/background.png')} style={styles.backgroundImage}/>
        <Animated.View
          style={[styles.loginView,
            {transform: [{translateY: this.state.bounceValue}]}]}
        >
              <Text style = {styles.loginTitleText}>Login</Text>
              <View style={styles.inputTextContainer}>
                  <Image
                      source={require('./images/icon_username.png')} //Change your icon image here
                      style={styles.ImageStyle}
                  />
                  <TextInput
                    style={styles.inputText}
                    placeholder="Email"
                    placeholderTextColor="#C9C8C9"
                    onChangeText={text => this.setState({email: text})}/>
              </View>
              <View style={styles.inputTextContainer}>
                  <Image
                      source={require('./images/icon_password.png')} //Change your icon image here
                      style={styles.ImageStyle}
                  />
                  <TextInput
                    style={styles.inputText}
                    placeholder="Password"
                    secureTextEntry={true}
                    placeholderTextColor="#C9C8C9"
                    onChangeText={text => this.setState({password: text})}/>
              </View>
              <View style={styles.buttonContainer}>
                  <AnimateLoadingButton
                      ref={c => (this.loadingButton = c)}
                      width= {200}
                      height={45}
                      title="SIGN IN"
                      titleFontSize={18}
                      titleWeight={'100'}
                      titleColor="#FFF"
                      backgroundColor="#D04236"
                      borderRadius={25}
                      onPress={this._onLoginClick.bind(this)}
                  />
              </View>
          </Animated.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  backgroundImage: {
    flex: 1,
    height: 'auto',
    width: 'auto',
    justifyContent: 'center'
  },
  loginView: {
    flex: 1,
    position: 'absolute',
    backgroundColor: '#fff',
    position: 'absolute',
    borderRadius: 10,
    width: '80%',
    height: 300,
    padding: 20,
    alignItems: "center",
    marginLeft: 40, //  Used for superview
    position: "absolute",
  },
  loginTitleText: {
    color: 'black',
    fontSize: 22,
    fontWeight: 'normal',
    textAlign: 'center',
    padding: 20,
  },
  inputTextContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: 40,
    borderRadius: 5,
    margin: 10,
    borderColor: '#d4d4d4',
    borderBottomWidth: 1,
  },
  ImageStyle: {
    padding: 10,
    margin: 10,
    height: 25,
    width: 25,
    resizeMode: 'stretch',
    alignItems: 'center',
  },
  inputText: {
    fontSize: 16,
    height: 50,
    color: 'black',
    width: '90%',
  },
  buttonContainer: {
    marginTop: 20,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 10,
    shadowOpacity: 0.25,
  },
});
