import React, {Component} from 'react';
import { View, Platform, Text, ScrollView, Image, StyleSheet } from 'react-native';
import { createStackNavigator, createDrawerNavigator, DrawerItems, SafeAreaView } from 'react-navigation';
import { Icon } from 'react-native-elements';

import Login from './LoginComponent';
import Home from './HomeComponent';
import SalesEnablement from './SalesEnablement';
import CEOSpeaksView from './CEOSpeaksView';

import SideMenu from 'react-native-side-menu';

// Custom Side Drawer
const CustomDrawerContentComponent = (props) => (
  <ScrollView>
    <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
      <View style={styles.drawerHeader}>
        <View style={{flex:1, paddingLeft: 20}}>
          <Image source={require('./images/placeholderImg.png')} style={styles.drawerImage} />
          <Text style={styles.drawerHeaderText}>Profile Text</Text>
          <Text style={styles.drawerEmailText}>Email</Text>
        </View>
      </View>
      <DrawerItems {...props} />
    </SafeAreaView>
  </ScrollView>
);

// Home Screen
const HomeNavigation = createStackNavigator({
    Home: {
      screen: Home,
      navigationOptions: ({ navigation }) => ({
        headerStyle: {
          backgroundColor: '#D04236'
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          color: '#fff'
        },
        headerLeft: <Icon name="menu" size={24}
                    color= 'white'
                    style={{padding: 8}}
                    onPress={ () => navigation.toggleDrawer() }
                  />
      })
    }
});

//  CEOSpeaksView Navigation
const CEOSpeaksNavigation = createStackNavigator({
    CEOSpeaksView:  CEOSpeaksView,
  },
  {
    initailRouteName: 'CEOSpeaks',
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: '#D04236'
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        color: '#fff'
      },
      headerLeft: <Icon name="menu" size={24}
                  color= 'white'
                  style={{padding: 8}}
                  onPress={ () => navigation.toggleDrawer() }
                />
    })
});

// SalesEnablement Screen
const SalesEnablementNavigation = createStackNavigator({
    SalesEnablement:  SalesEnablement,
  },
  {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: '#D04236',
        borderBottomWidth: 0
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        color: '#fff'
      },
      headerLeft: <Icon name="menu" size={24}
                  color= 'white'
                  style={{padding: 8}}
                  onPress={ () => navigation.toggleDrawer() }
                />
    })
});

const MainNavigation = createDrawerNavigator({
    Home: {
      screen: HomeNavigation,
      navigationOptions: {
        title: 'Home',
        drawerLabel: 'Home',
      /*  drawerIcon: ({ tintColor, focused }) => (
            <Icon
              name='home'
              type='font-awesome'
              size={24}
              color={tintColor}
            />
          ),
          */
      },
      initialRouteName: 'Home',
    },
    CEOSpeaksView: {
      screen: CEOSpeaksNavigation,
      navigationOptions: {
        title: 'CEOSpeaks',
        drawerLabel: 'CEO Speaks',
        /*drawerIcon: ({ tintColor, focused }) => (
            <Icon
              name='anouncement'
              type='font-awesome'
              size={24}
              color={tintColor}
            />
          ), */
      }
    },
    SalesEnablement: {
      screen: SalesEnablementNavigation,
      navigationOptions: {
        title: 'Sales Enablement',
        drawerLabel: 'Sales Enablement',
      }
    },
  }, {
      drawerBackgroundColor: '#fff',
      contentComponent: CustomDrawerContentComponent,
      contentOptions: {
        labelStyle: {
          fontWeight: '500',
          fontSize: 18,
        },
        activeTintColor: 'red',
      },
});

// Login Screen
const LoginNavigation = createStackNavigator({
    Login: Login,
    MainNavigation: MainNavigation
  }, {
      navigationOptions: ({ navigation }) => ({
          headerStyle: {
            display:"none"
          },
          animationEnabled: false,
      }),
  });

class Main extends Component {

  render() {
    return (
        <LoginNavigation/>
    );
  }
}

export default Main;


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  drawerHeader: {
    height: 160,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row'
  },
  drawerHeaderText: {
    color: 'black',
    fontSize: 22,
    paddingVertical: 5,
  },
  drawerEmailText: {
    color: 'black',
    fontSize: 18,
    paddingBottom: 20,
  },
  drawerImage: {
    width: 80,
    height: 80,
  },
  drawerContentOptions: {
    fontSize: 18,
    fontWeight: 'normal',
  }
});
