import React, { Component } from 'react';
import {
  StyleSheet,
  FlatList,
  ActivityIndicator,
  Image,
  TouchableOpacity,
  Text,
  View,
  Alert,
} from 'react-native';

export default class SalesEnablement extends Component {
  constructor(props) {
     super(props);

     this.state = { GridViewItems: [
       {key: 'Lead'},
       {key: 'Mettings'},
       {key: 'Collaborations'},
       {key: 'Notes'},
       {key: 'Cross Refrencing'},
       {key: 'Scan a \n Visiting Card'}
     ]}
   }

   static navigationOptions = {
     title: 'Sales Enablement'
   };

   _gridViewItemClicked (item) {
      Alert.alert(item);
  }

  render() {
      return (
          <View style={styles.MainContainer}>
              <View style = {styles.headerView}/>
              <View style = {styles.flatListContainer}>
                <FlatList
                    data={ this.state.GridViewItems }
                    renderItem={({item}) =>
                      <View style={styles.GridViewBlockStyle}>
                        <Image
                            source={require('./images/team.png')} //Change your icon image here
                            style={styles.GridIconStyle}
                          />
                        <Text style={styles.GridViewInsideTextItemStyle} onPress={this._gridViewItemClicked.bind(this, item.key)} > {item.key} </Text>
                      </View>}
                    numColumns={2}/>
              </View>
          </View>
   );
 }
}


const styles = StyleSheet.create({

    MainContainer: {
        flex:1,
        backgroundColor: '#F6F5F6',
        alignItems: 'center',
    },
    headerView: {
        width: '100%',
        height: 100,
        backgroundColor: '#D04236',
    },
    flatListContainer: {
      position: 'absolute',
      width: '92%',
      paddingTop: 50,
    },
    GridViewBlockStyle: {
        flex:1,
        alignItems: 'center',
        height: 160,
        margin: 10,
        backgroundColor: '#fff',
        borderRadius: 5,
        shadowColor: '#000000',
        shadowOffset: {
          width: 0,
          height: 1
        },
        shadowRadius: 1,
        shadowOpacity: 0.05,
    },
    GridIconStyle : {
      marginTop:30,
      height: 70,
      width: 70,
    },
    GridViewInsideTextItemStyle: {
        color: '#fff',
        padding: 10,
        fontSize: 18,
        justifyContent: 'center',
        textAlign: 'center',
        color: 'black',
    },
    GridBackgroundImage: {
      justifyContent: 'center',
    }
});
