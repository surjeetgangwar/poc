export const POSTS =
    [
        {
        id: 0,
        name:'Uthappizza',
        image: 'images/uthappizza.png',
        category: 'mains',
        likes:'5',
        commnets:'10',
        featured: true,
        description:'A unique combination of Indian Uthappam (pancake) and Italian pizza, topped with Cerignola olives, ripe vine cherry tomatoes, Vidalia onion, Guntur chillies and Buffalo Paneer.'
        },
        {
        id: 1,
        name:'Zucchipakoda',
        image: 'images/zucchipakoda.png',
        category: 'appetizer',
        likes:'1',
        commnets:'5',
        featured: false,
        description:'Deep fried Zucchini coated with mildly spiced Chickpea flour batter accompanied with a sweet-tangy tamarind sauce'
        },
        {
        id: 2,
        name:'Vadonut',
        image: 'images/vadonut.png',
        category: 'appetizer',
        likes:'6',
        commnets:'2',
        featured: false,
        description:'A quintessential ConFusion experience, is it a vada or is it a donut?'
        },
        {
        id: 3,
        name:'ElaiCheese Cake',
        image: 'images/elaicheesecake.png',
        category: 'dessert',
        likes:'7',
        commnets:'12',
        featured: false,
        description:'A delectable, semi-sweet New York Style Cheese Cake, with Graham cracker crust and spiced with Indian cardamoms'
        }
    ];
